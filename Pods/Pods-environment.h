
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// AFNetworking
#define COCOAPODS_POD_AVAILABLE_AFNetworking
#define COCOAPODS_VERSION_MAJOR_AFNetworking 2
#define COCOAPODS_VERSION_MINOR_AFNetworking 2
#define COCOAPODS_VERSION_PATCH_AFNetworking 0

// AFNetworking/NSURLConnection
#define COCOAPODS_POD_AVAILABLE_AFNetworking_NSURLConnection
#define COCOAPODS_VERSION_MAJOR_AFNetworking_NSURLConnection 2
#define COCOAPODS_VERSION_MINOR_AFNetworking_NSURLConnection 2
#define COCOAPODS_VERSION_PATCH_AFNetworking_NSURLConnection 0

// AFNetworking/NSURLSession
#define COCOAPODS_POD_AVAILABLE_AFNetworking_NSURLSession
#define COCOAPODS_VERSION_MAJOR_AFNetworking_NSURLSession 2
#define COCOAPODS_VERSION_MINOR_AFNetworking_NSURLSession 2
#define COCOAPODS_VERSION_PATCH_AFNetworking_NSURLSession 0

// AFNetworking/Reachability
#define COCOAPODS_POD_AVAILABLE_AFNetworking_Reachability
#define COCOAPODS_VERSION_MAJOR_AFNetworking_Reachability 2
#define COCOAPODS_VERSION_MINOR_AFNetworking_Reachability 2
#define COCOAPODS_VERSION_PATCH_AFNetworking_Reachability 0

// AFNetworking/Security
#define COCOAPODS_POD_AVAILABLE_AFNetworking_Security
#define COCOAPODS_VERSION_MAJOR_AFNetworking_Security 2
#define COCOAPODS_VERSION_MINOR_AFNetworking_Security 2
#define COCOAPODS_VERSION_PATCH_AFNetworking_Security 0

// AFNetworking/Serialization
#define COCOAPODS_POD_AVAILABLE_AFNetworking_Serialization
#define COCOAPODS_VERSION_MAJOR_AFNetworking_Serialization 2
#define COCOAPODS_VERSION_MINOR_AFNetworking_Serialization 2
#define COCOAPODS_VERSION_PATCH_AFNetworking_Serialization 0

// AFNetworking/UIKit
#define COCOAPODS_POD_AVAILABLE_AFNetworking_UIKit
#define COCOAPODS_VERSION_MAJOR_AFNetworking_UIKit 2
#define COCOAPODS_VERSION_MINOR_AFNetworking_UIKit 2
#define COCOAPODS_VERSION_PATCH_AFNetworking_UIKit 0

// BEMSimpleLineGraph
#define COCOAPODS_POD_AVAILABLE_BEMSimpleLineGraph
#define COCOAPODS_VERSION_MAJOR_BEMSimpleLineGraph 2
#define COCOAPODS_VERSION_MINOR_BEMSimpleLineGraph 0
#define COCOAPODS_VERSION_PATCH_BEMSimpleLineGraph 1

// ColorUtils
#define COCOAPODS_POD_AVAILABLE_ColorUtils
#define COCOAPODS_VERSION_MAJOR_ColorUtils 1
#define COCOAPODS_VERSION_MINOR_ColorUtils 1
#define COCOAPODS_VERSION_PATCH_ColorUtils 2

// CorePlot
#define COCOAPODS_POD_AVAILABLE_CorePlot
#define COCOAPODS_VERSION_MAJOR_CorePlot 1
#define COCOAPODS_VERSION_MINOR_CorePlot 4
#define COCOAPODS_VERSION_PATCH_CorePlot 0

// DLAlertView
#define COCOAPODS_POD_AVAILABLE_DLAlertView
#define COCOAPODS_VERSION_MAJOR_DLAlertView 1
#define COCOAPODS_VERSION_MINOR_DLAlertView 0
#define COCOAPODS_VERSION_PATCH_DLAlertView 0

// DOSingleton
#define COCOAPODS_POD_AVAILABLE_DOSingleton
#define COCOAPODS_VERSION_MAJOR_DOSingleton 0
#define COCOAPODS_VERSION_MINOR_DOSingleton 0
#define COCOAPODS_VERSION_PATCH_DOSingleton 2

// FBGlowLabel
#define COCOAPODS_POD_AVAILABLE_FBGlowLabel
#define COCOAPODS_VERSION_MAJOR_FBGlowLabel 0
#define COCOAPODS_VERSION_MINOR_FBGlowLabel 0
#define COCOAPODS_VERSION_PATCH_FBGlowLabel 1

// HexColors
#define COCOAPODS_POD_AVAILABLE_HexColors
#define COCOAPODS_VERSION_MAJOR_HexColors 2
#define COCOAPODS_VERSION_MINOR_HexColors 2
#define COCOAPODS_VERSION_PATCH_HexColors 1

// MBProgressHUD
#define COCOAPODS_POD_AVAILABLE_MBProgressHUD
#define COCOAPODS_VERSION_MAJOR_MBProgressHUD 0
#define COCOAPODS_VERSION_MINOR_MBProgressHUD 8
#define COCOAPODS_VERSION_PATCH_MBProgressHUD 0

// MHRotaryKnob
#define COCOAPODS_POD_AVAILABLE_MHRotaryKnob
#define COCOAPODS_VERSION_MAJOR_MHRotaryKnob 1
#define COCOAPODS_VERSION_MINOR_MHRotaryKnob 1
#define COCOAPODS_VERSION_PATCH_MHRotaryKnob 0

// MagicPie
#define COCOAPODS_POD_AVAILABLE_MagicPie
#define COCOAPODS_VERSION_MAJOR_MagicPie 1
#define COCOAPODS_VERSION_MINOR_MagicPie 0
#define COCOAPODS_VERSION_PATCH_MagicPie 0

// MagicalRecord
#define COCOAPODS_POD_AVAILABLE_MagicalRecord
#define COCOAPODS_VERSION_MAJOR_MagicalRecord 2
#define COCOAPODS_VERSION_MINOR_MagicalRecord 2
#define COCOAPODS_VERSION_PATCH_MagicalRecord 0

// MagicalRecord/Core
#define COCOAPODS_POD_AVAILABLE_MagicalRecord_Core
#define COCOAPODS_VERSION_MAJOR_MagicalRecord_Core 2
#define COCOAPODS_VERSION_MINOR_MagicalRecord_Core 2
#define COCOAPODS_VERSION_PATCH_MagicalRecord_Core 0

// Mantle
#define COCOAPODS_POD_AVAILABLE_Mantle
#define COCOAPODS_VERSION_MAJOR_Mantle 1
#define COCOAPODS_VERSION_MINOR_Mantle 4
#define COCOAPODS_VERSION_PATCH_Mantle 0

// Mantle/extobjc
#define COCOAPODS_POD_AVAILABLE_Mantle_extobjc
#define COCOAPODS_VERSION_MAJOR_Mantle_extobjc 1
#define COCOAPODS_VERSION_MINOR_Mantle_extobjc 4
#define COCOAPODS_VERSION_PATCH_Mantle_extobjc 0

// NZAlertView
#define COCOAPODS_POD_AVAILABLE_NZAlertView
#define COCOAPODS_VERSION_MAJOR_NZAlertView 0
#define COCOAPODS_VERSION_MINOR_NZAlertView 0
#define COCOAPODS_VERSION_PATCH_NZAlertView 9

// PNChart
#define COCOAPODS_POD_AVAILABLE_PNChart
#define COCOAPODS_VERSION_MAJOR_PNChart 0
#define COCOAPODS_VERSION_MINOR_PNChart 3
#define COCOAPODS_VERSION_PATCH_PNChart 3

// PulsingHalo
#define COCOAPODS_POD_AVAILABLE_PulsingHalo
#define COCOAPODS_VERSION_MAJOR_PulsingHalo 0
#define COCOAPODS_VERSION_MINOR_PulsingHalo 0
#define COCOAPODS_VERSION_PATCH_PulsingHalo 1

// RNFrostedSidebar
#define COCOAPODS_POD_AVAILABLE_RNFrostedSidebar
#define COCOAPODS_VERSION_MAJOR_RNFrostedSidebar 0
#define COCOAPODS_VERSION_MINOR_RNFrostedSidebar 2
#define COCOAPODS_VERSION_PATCH_RNFrostedSidebar 0

// SAMCache
#define COCOAPODS_POD_AVAILABLE_SAMCache
#define COCOAPODS_VERSION_MAJOR_SAMCache 0
#define COCOAPODS_VERSION_MINOR_SAMCache 3
#define COCOAPODS_VERSION_PATCH_SAMCache 0

// SSToolkit
#define COCOAPODS_POD_AVAILABLE_SSToolkit
#define COCOAPODS_VERSION_MAJOR_SSToolkit 1
#define COCOAPODS_VERSION_MINOR_SSToolkit 0
#define COCOAPODS_VERSION_PATCH_SSToolkit 4

// SpinKit
#define COCOAPODS_POD_AVAILABLE_SpinKit
#define COCOAPODS_VERSION_MAJOR_SpinKit 1
#define COCOAPODS_VERSION_MINOR_SpinKit 0
#define COCOAPODS_VERSION_PATCH_SpinKit 1

// TSMessages
#define COCOAPODS_POD_AVAILABLE_TSMessages
#define COCOAPODS_VERSION_MAJOR_TSMessages 0
#define COCOAPODS_VERSION_MINOR_TSMessages 9
#define COCOAPODS_VERSION_PATCH_TSMessages 5

// UICountingLabel
#define COCOAPODS_POD_AVAILABLE_UICountingLabel
#define COCOAPODS_VERSION_MAJOR_UICountingLabel 1
#define COCOAPODS_VERSION_MINOR_UICountingLabel 0
#define COCOAPODS_VERSION_PATCH_UICountingLabel 0

// UIImage-Helpers
#define COCOAPODS_POD_AVAILABLE_UIImage_Helpers
#define COCOAPODS_VERSION_MAJOR_UIImage_Helpers 0
#define COCOAPODS_VERSION_MINOR_UIImage_Helpers 0
#define COCOAPODS_VERSION_PATCH_UIImage_Helpers 2

