//
//  DemoTableViewController.m
//  ZGParallelViewForTable
//
//  Created by Kyle Fang on 1/7/13.
//  Copyright (c) 2013 kylefang. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "DemoTableViewController.h"
#import "UITableView+ZGParallelView.h"
#import <TSMessages/TSMessage.h>
#import "NZAlertView.h"
#import "RTSpinKitView.h"
#import "GradientBackgroundView.h"

@interface DemoTableViewController ()
@property (strong, nonatomic) IBOutlet UIView *awesomeZG;
@property (nonatomic) BOOL usedPageControl;

@property(nonatomic, strong)NSArray *serviceItems;


@end

@implementation DemoTableViewController

MBProgressHUD *hud ;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self customizeUI];
    
    [self.tableView addParallelViewWithUIView:self.awesomeZG withDisplayRadio:0.7 headerViewStyle:ZGScrollViewStyleCutOffAtMax];
    
    [[DNSDiscovery sharedInstance] setDelegate:self];
    [[DNSDiscovery sharedInstance] startSearch];
    [self ServiceListUpdated];
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (scrollView == self.tableView) {
        [self.tableView updateParallelViewWithOffset:scrollView.contentOffset];
    }
}


- (void)customizeUI {
    self.tableView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"BackgroundImage"]];
}


- (void)ServiceListUpdated {
    
    self.serviceItems = [[DNSDiscovery sharedInstance] getAllServices];
    [self.tableView reloadData];
}


- (void)setupHub {
    RTSpinKitView *spinner = [[RTSpinKitView alloc] initWithStyle:RTSpinKitViewStyleWave color:[UIColor whiteColor]];
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.square = YES;
    hud.mode = MBProgressHUDModeCustomView;
    hud.customView = spinner;
    hud.labelText = NSLocalizedString(@"解析中", @"Loading");
    [spinner startAnimating];
}

- (IBAction)inputedRemoteURL:(UIButton *)sender {
    DLAVAlertView *alertView = [[DLAVAlertView alloc] initWithTitle:@"服务器地址" message:nil delegate:nil cancelButtonTitle:@"确定 " otherButtonTitles:@"取消", nil];
    NSString *currentURL = [[SAMCache sharedCache] objectForKey:RemoteServerAddress];
    [alertView addTextFieldWithText:currentURL placeholder:@"请输入地址..."];
    [alertView showWithCompletion:^(DLAVAlertView *alertView, NSInteger buttonIndex) {
        if (buttonIndex == 0) {    //确定
            NSString *theUrl = [alertView textFieldTextAtIndex:0];
            if ([theUrl isEqualToString:@""]) {
                return ;
            }
            [[SAMCache sharedCache] setObject:[alertView textFieldTextAtIndex:0] forKey:RemoteServerAddress];
            [self performSelector:@selector(dismissSelf) withObject:nil afterDelay:1];
        }
    }];
}

#pragma - tableViewDatasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"服务器列表";
}

- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    return @"列出现在网络环境中所有可用的事实数据库服务地址";
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.serviceItems count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    cell = [self.tableView dequeueReusableCellWithIdentifier:@"ServerCell" forIndexPath:indexPath];
    
    NSNetService *netService = [self.serviceItems objectAtIndex:indexPath.row];
    cell.textLabel.text = [netService name];
    cell.detailTextLabel.text = [netService type];
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (!hud) {
        [self setupHub];
    }
    [hud show:YES];
    
    NSNetService *netService = [self.serviceItems objectAtIndex:indexPath.row];
    [[DNSDiscovery sharedInstance] resolveNetService:netService];
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}


#pragma - DNSDiscoveryDelegate


- (void)NetServiceResolved:(NSString *)theURL {

    //[TSMessage setDefaultViewController:self];
    //[TSMessage showNotificationWithTitle:@"OK!" subtitle:theURL type:TSMessageNotificationTypeMessage];

    [hud hide:YES];
    [[SAMCache sharedCache] setObject:theURL forKey:RemoteServerAddress];
    NZAlertView *alert = [[NZAlertView alloc] initWithStyle:NZAlertStyleSuccess title:@"解析成功！" message:theURL];
    [alert show];
    [self performSelector:@selector(dismissSelf) withObject:nil afterDelay:1];
}


- (void)NetServiceResolvedError:(NSString *)errorMsg {

    [hud hide:YES];
    //[TSMessage showNotificationWithTitle:@"Error!" subtitle:errorMsg type:TSMessageNotificationTypeError];
    NZAlertView *alert = [[NZAlertView alloc] initWithStyle:NZAlertStyleError title:@"解析失败!" message:errorMsg];
    [alert show];

}


- (void)dismissSelf
{
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

@end
