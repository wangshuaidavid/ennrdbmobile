//
//  EnnRDBTagDetailViewController.m
//  EnnRDBMobile
//
//  Created by wang david on 14-3-5.
//  Copyright (c) 2014年 Enn. All rights reserved.
//

#import "EnnRDBTagDetailViewController.h"
#import "RWKnobControl.h"
#import "FBGlowLabel.h"

@interface EnnRDBTagDetailViewController () {
    NSTimer *timer ;
    RWKnobControl *_knobControl;

    
}

@property (weak, nonatomic) IBOutlet UIView *knobPlaceHolder;
@property (weak, nonatomic) IBOutlet FBGlowLabel *valueLabel;
@property (weak, nonatomic) IBOutlet UILabel *tagIDLabel;
@property (weak, nonatomic) IBOutlet UILabel *tagNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *valueTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *timestampLabel;
@property (weak, nonatomic) IBOutlet UILabel *qtyLabel;

@property (weak, nonatomic) IBOutlet UILabel *knobValueLabel;

@end


@implementation EnnRDBTagDetailViewController


- (void)customUI
{
    //http://www.raywenderlich.com/56885/custom-control-for-ios-tutorial-a-reusable-knob
    _knobControl = [[RWKnobControl alloc] initWithFrame:self.knobPlaceHolder.bounds];
    [self.knobPlaceHolder addSubview:_knobControl];
    
    _knobControl.lineWidth = 3.0;
    _knobControl.pointerLength = 15.0;
    _knobControl.maximumValue = 11;
    _knobControl.minimumValue = -6;
    self.view.tintColor = [UIColor orangeColor];
    [_knobControl addObserver:self forKeyPath:@"value" options:0 context:NULL];
    
}

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context
{
    if(object == _knobControl && [keyPath isEqualToString:@"value"]) {
        //self.valueLabel.text = [NSString stringWithFormat:@"%0.2f", _knobControl.value];
        self.knobValueLabel.text = [NSString stringWithFormat:@"%0.2f", _knobControl.value];
    }
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    [self customUI];
    
    timer = [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(goGetValue) userInfo:nil repeats:YES];
    
    NSNumber *tagid = [self.currentTagDict objectForKey:@"TagId"];
    self.tagIDLabel.text = [tagid stringValue];
    self.tagNameLabel.text = [self.currentTagDict objectForKey:@"Name"];
    
    
    self.valueLabel.glowSize = 20;
    self.valueLabel.glowColor = [UIColor blueColor];
    
    self.valueLabel.innerGlowSize = 4;
    self.valueLabel.innerGlowColor = [UIColor blueColor];
    
}

- (void)viewWillDisappear:(BOOL)animated {
    [timer invalidate];
}

- (void)goGetValue
{
    NSURL *url = [[DNSDiscovery sharedInstance] getTagDetailURL:[self.currentTagDict valueForKey:@"Name"]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *e = nil;
        NSDictionary *dataDict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:&e];
        
        NSString *value = [dataDict valueForKey:@"Value"];
        
        self.valueLabel.text = value;
        
        if (!_knobControl.isKnobTouched) {
            [_knobControl setValue:[value intValue] animated:YES];
        }
        
        self.valueTypeLabel.text = [dataDict valueForKey:@"ValueType"];
        self.timestampLabel.text = [dataDict valueForKey:@"Timestamp"];
        self.qtyLabel.text = [dataDict valueForKey:@"Quality"];

    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        [timer invalidate];
        NSLog(@"%@", [error localizedDescription]);
    }];

    [operation start];
}

@end
