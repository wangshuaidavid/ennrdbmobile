//
//  EnnRDBTagDetailViewController.h
//  EnnRDBMobile
//
//  Created by wang david on 14-3-5.
//  Copyright (c) 2014年 Enn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EnnRDBTagDetailViewController : UIViewController

@property(nonatomic, strong) NSDictionary *currentTagDict;
@end
