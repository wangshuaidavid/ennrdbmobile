//
//  EnnRDBViewController.h
//  EnnRDBMobile
//
//  Created by wang david on 14-2-17.
//  Copyright (c) 2014年 Enn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RNFrostedSidebar.h"

@interface EnnRDBViewController : UIViewController <RNFrostedSidebarDelegate>
@property (weak, nonatomic) IBOutlet UIBarButtonItem *DetailItemButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *scanItemButton;

@end
