//
//  EnnRDBDetailViewController.h
//  EnnRDBMobile
//
//  Created by wang david on 14-2-27.
//  Copyright (c) 2014年 Enn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BEMSimpleLineGraphView.h"

@interface EnnRDBDetailViewController : UIViewController <BEMSimpleLineGraphDelegate>

@property (weak, nonatomic) IBOutlet BEMSimpleLineGraphView *lineGraphView;

@end
