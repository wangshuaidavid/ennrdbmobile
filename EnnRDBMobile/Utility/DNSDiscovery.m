//
//  DNSDiscovery.m
//  EnvRemote
//
//  Created by wang david on 13-4-9.
//  Copyright (c) 2013年 Enn. All rights reserved.
//

#import "DNSDiscovery.h"


#define InitialDomain  @"local"
#define EnnServiceType @"_ennremote._tcp"

@interface DNSDiscovery () {
    NSMutableArray* services;
	NSNetServiceBrowser* netServiceBrowser;
	NSNetService* currentResolve;
}
@end

@implementation DNSDiscovery

static NSString *GenDataURLTemplate = @"http://%@/GenData.aspx";
static NSString *TagInfoURLTemplate = @"http://%@";
static NSString *TagDetailURLTemplate = @"http://%@/About.aspx?tagName=%@";



- (BOOL)isBaseURLExists {
    return [[SAMCache sharedCache] objectExistsForKey:RemoteServerAddress];
}


- (NSURL *)getGenDataURL {
    
    if (![self isBaseURLExists]) {
        return nil;
    }
    NSString *baseURL = [[SAMCache sharedCache] objectForKey:RemoteServerAddress];
    NSString *genURL = [NSString stringWithFormat:GenDataURLTemplate, baseURL];
    return [NSURL URLWithString:genURL];
}


- (NSURL *)getTagInfoURL {
    
    if (![self isBaseURLExists]) {
        return nil;
    }
    NSString *baseURL = [[SAMCache sharedCache] objectForKey:RemoteServerAddress];
    NSString *genURL = [NSString stringWithFormat:TagInfoURLTemplate, baseURL];
    return [NSURL URLWithString:genURL];
}


- (NSURL *)getTagDetailURL:(NSString *)theTagName {
    
    if (![self isBaseURLExists]) {
        return nil;
    }
    NSString *baseURL = [[SAMCache sharedCache] objectForKey:RemoteServerAddress];
    NSString *genURL = [NSString stringWithFormat:TagDetailURLTemplate, baseURL, theTagName];
    return [NSURL URLWithString:genURL];
}



- (void)startSearch {
  
  if (!netServiceBrowser) {
    netServiceBrowser = [[NSNetServiceBrowser alloc] init];
    services = [NSMutableArray arrayWithCapacity:10];
  }
  netServiceBrowser.delegate = self;

  [netServiceBrowser searchForServicesOfType:EnnServiceType inDomain:InitialDomain];
}

    
- (void)stopCurrentResolve {
	[currentResolve stop];
	currentResolve = nil;
}

- (void)resolveNetService:(NSNetService *)theNetService
{
    currentResolve = theNetService;
    [currentResolve setDelegate:self];
    [currentResolve resolveWithTimeout:5];
}


- (NSArray *)getAllServices
{
    return services;
}

//delegates

- (void)netServiceBrowser:(NSNetServiceBrowser*)netServiceBrowser didRemoveService:(NSNetService*)service moreComing:(BOOL)moreComing {
	if (currentResolve && [service isEqual:currentResolve]) {
		[self stopCurrentResolve];
	}
	[services removeObject:service];
	
  
	if (!moreComing) {
        [self.delegate ServiceListUpdated];
	}
}


- (void)netServiceBrowser:(NSNetServiceBrowser*)netServiceBrowser didFindService:(NSNetService*)service moreComing:(BOOL)moreComing {
	[services addObject:service];
	if (!moreComing) {
        [self.delegate ServiceListUpdated];
	}
}


// This should never be called, since we resolve with a timeout of 0.0, which means indefinite
- (void)netService:(NSNetService *)sender didNotResolve:(NSDictionary *)errorDict {
	[self stopCurrentResolve];
    [self.delegate NetServiceResolvedError:@"解析失败！"];
}


- (void)netServiceDidResolveAddress:(NSNetService *)service {
    
	[self stopCurrentResolve];
  
    NSString *s = [[NSString alloc] initWithData:service.TXTRecordData encoding:NSUTF8StringEncoding];
    NSString *foundedURL = [s componentsSeparatedByString:@"="].lastObject;
    [self.delegate NetServiceResolved:foundedURL];
    
}

@end
