//
//  GradientBackgroundView.m
//  SimpleWarehouseManagement
//
//  Created by wangshuaidavid on 11-5-16.
//  Copyright 2011 Mobilogics. All rights reserved.
//

#import "GradientBackgroundView.h"


@implementation GradientBackgroundView

+ (Class)layerClass
{
	return [CAGradientLayer class];
}

- (void)awakeFromNib {
	
	CAGradientLayer *gradientLayer = (CAGradientLayer *)self.layer;
	
	//gradientLayer.locations = [NSArray arrayWithObjects:[NSNumber numberWithDouble:0.3], [NSNumber numberWithDouble:0.7],nil];
	gradientLayer.colors = [NSArray arrayWithObjects:(id)[UIColor whiteColor].CGColor, (id)[UIColor lightGrayColor].CGColor,nil];
	
	[gradientLayer setDelegate:self];
	[gradientLayer setNeedsDisplay];
	self.backgroundColor = [UIColor clearColor];
}


- (void)drawLayer:(CALayer *)layer inContext:(CGContextRef)ctx {
	
	float borderWidth = 1.0f;
	
	CGContextSetRGBFillColor(ctx, 0.333, 0.333, 0.333, 1);
	CGContextFillRect(ctx, CGRectMake(0, (layer.bounds.size.height - borderWidth), layer.bounds.size.width, borderWidth));
}


@end
