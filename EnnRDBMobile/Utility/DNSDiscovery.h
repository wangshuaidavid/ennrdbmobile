//
//  DNSDiscovery.h
//  EnvRemote
//
//  Created by wang david on 13-4-9.
//  Copyright (c) 2013年 Enn. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DOSingleton.h"
@protocol DNSDiscoveryDelegate;

@interface DNSDiscovery : DOSingleton <NSNetServiceBrowserDelegate, NSNetServiceDelegate>

@property(nonatomic, strong)id<DNSDiscoveryDelegate> delegate;

- (void)startSearch ;
- (void)resolveNetService:(NSNetService *)theNetService;

- (NSArray *)getAllServices;


- (NSURL *)getGenDataURL;
- (NSURL *)getTagInfoURL;
- (NSURL *)getTagDetailURL:(NSString *)theTagName;

@end


@protocol DNSDiscoveryDelegate <NSObject>

- (void)ServiceListUpdated;
- (void)NetServiceResolved:(NSString *)theURL;
- (void)NetServiceResolvedError:(NSString *)errorMsg;

@end