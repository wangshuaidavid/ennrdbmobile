//
//  EnnRDBAppDelegate.h
//  EnnRDBMobile
//
//  Created by wang david on 14-2-17.
//  Copyright (c) 2014年 Enn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EnnRDBAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
