//
//  EnnRDBDetailViewController.m
//  EnnRDBMobile
//
//  Created by wang david on 14-2-27.
//  Copyright (c) 2014年 Enn. All rights reserved.
//

#import "EnnRDBDetailViewController.h"
#import "RWKnobControl.h"

@interface EnnRDBDetailViewController () {
}

@property (nonatomic, strong) NSArray *mockupData ;
@property (nonatomic, strong) NSArray *mockupString ;

@property (weak, nonatomic) IBOutlet UILabel *tagLable;
@property (weak, nonatomic) IBOutlet UILabel *tagValue;

@end


@implementation EnnRDBDetailViewController


- (void)customUI {

    self.mockupData = @[@"-1", @"4", @"9", @"0", @"1", @"5", @"2", @"6", @"-3"];
    self.mockupString = @[@"T1", @"T2", @"T3", @"T4", @"T5", @"T6", @"T7", @"T8", @"T9"];

}


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self.lineGraphView setEnableTouchReport:YES];
    [self.lineGraphView setColorLine:[UIColor lightGrayColor]];
    [self.lineGraphView setColorBottom:[UIColor colorWithRed:0/255.0 green:100/255.0 blue:169/255.0 alpha:0.7]];
    [self customUI];
}

- (IBAction)backToNavi:(UIBarButtonItem *)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}



#pragma - BEMSimpleLineGraphDelegate

- (int)numberOfPointsInGraph {

    return [self.mockupData count];
}


- (float)valueForIndex:(NSInteger)index{
    return [[self.mockupData objectAtIndex:index] floatValue];
}


- (int)numberOfGapsBetweenLabels {
    return 1;
}



- (NSString *)labelOnXAxisForIndex:(NSInteger)index {
    return [self.mockupString objectAtIndex:index];
}

- (void)didTouchGraphWithClosestIndex:(int)index {
    
    self.tagLable.text = [self.mockupString objectAtIndex:index];
    self.tagValue.text = [self.mockupData objectAtIndex:index];
}
@end
