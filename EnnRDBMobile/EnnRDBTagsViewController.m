//
//  EnnRDBTagsViewController.m
//  EnnRDBMobile
//
//  Created by wang david on 14-3-5.
//  Copyright (c) 2014年 Enn. All rights reserved.
//

#import "EnnRDBTagsViewController.h"
#import "EnnRDBTagDetailViewController.h"
#import "RTSpinKitView.h"
#import "NZAlertView.h"

@interface EnnRDBTagsViewController () {
    
    MBProgressHUD *hud ;
}

@property(nonatomic, strong)NSArray *dataArray ;
@end

@implementation EnnRDBTagsViewController



- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.title = @"标签列表";
    
    
}

- (void)viewDidAppear:(BOOL)animated {
    
    [super viewDidAppear:animated];
    self.tableView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"BackgroundImage"]];
    
    NSURL *url = [[DNSDiscovery sharedInstance] getTagInfoURL];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString *s = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
        NSString *formatedS = [NSString stringWithFormat:@"[%@]", s];
        NSError *e = nil;
        NSArray *da = [NSJSONSerialization JSONObjectWithData:[formatedS dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableLeaves error:&e];
        
        self.dataArray = da;
        
        [self.tableView reloadData];
        [hud hide:YES];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [hud hide:YES];
        
        NZAlertView *alert = [[NZAlertView alloc] initWithStyle:NZAlertStyleError title:@"发生错误" message:[error localizedDescription]];
        [alert show];
    }];
    
    if (!hud) {
        [self setupHub];
    }
    [hud show:YES];
    
    [operation start];
}


- (void)setupHub {
    RTSpinKitView *spinner = [[RTSpinKitView alloc] initWithStyle:RTSpinKitViewStylePulse color:[UIColor whiteColor]];
    hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.square = YES;
    hud.mode = MBProgressHUDModeCustomView;
    hud.customView = spinner;
    hud.labelText = NSLocalizedString(@"加载中", @"Loading");
    [spinner startAnimating];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.dataArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"TagCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    NSDictionary *oneTag = [self.dataArray objectAtIndex:indexPath.row];
    
    cell.textLabel.text = [oneTag valueForKey:@"Name"];
    
    cell.detailTextLabel.text = [[oneTag valueForKey:@"TagId"] description];
    cell.tag = indexPath.row;
    return cell;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.destinationViewController isKindOfClass:[EnnRDBTagDetailViewController class]]) {
        
        UITableViewCell *cell = (UITableViewCell *)sender;
        NSDictionary *d = [self.dataArray objectAtIndex:cell.tag];
        [segue.destinationViewController setCurrentTagDict:d];
    }
}

 

@end
