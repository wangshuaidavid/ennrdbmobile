//
//  ParallelHeaderView.h
//  EnnRDBMobile
//
//  Created by wang david on 14-2-20.
//  Copyright (c) 2014年 Enn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ParallelHeaderView : UIView
@property (weak, nonatomic) IBOutlet UIButton *inputURLButton;

@end
