//
//  ParallelHeaderView.m
//  EnnRDBMobile
//
//  Created by wang david on 14-2-20.
//  Copyright (c) 2014年 Enn. All rights reserved.
//

#import "ParallelHeaderView.h"
#import "PulsingHaloLayer.h"
#import "ColorUtils.h"
#import "UIButton+Bootstrap.h"


@implementation ParallelHeaderView

+ (Class)layerClass
{
	return [CAGradientLayer class];
}

-(void)awakeFromNib
{
    
    PulsingHaloLayer *ph = [PulsingHaloLayer layer];
    ph.radius = 150;
    ph.position = CGPointMake(self.center.x, self.center.y - 16);
    ph.backgroundColor = [UIColor whiteColor].CGColor;
    [self.layer addSublayer:ph];
    
    
	CAGradientLayer *gradientLayer = (CAGradientLayer *)self.layer;
	gradientLayer.colors =
	[NSArray arrayWithObjects:
	 (id)[UIColor colorWithRed:95/255.0 green:183/255.0 blue:242/255.0 alpha:1.0].CGColor,
	 (id)[UIColor colorWithRed:0/255.0 green:100/255.0 blue:169/255.0 alpha:1.0].CGColor,
	 nil];

    CALayer *lineLayer = [CALayer layer];
    [lineLayer setFrame:CGRectMake(0, CGRectGetHeight(self.layer.frame) - 1, CGRectGetWidth(self.layer.frame), 1)];
    [lineLayer setBackgroundColor:[UIColor darkGrayColor].CGColor];
    [self.layer addSublayer:lineLayer];
	self.backgroundColor = [UIColor clearColor];
 
    [self.inputURLButton infoStyle];
}


@end
