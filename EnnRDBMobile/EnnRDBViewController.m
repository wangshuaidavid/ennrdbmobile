//
//  EnnRDBViewController.m
//  EnnRDBMobile
//
//  Created by wang david on 14-2-17.
//  Copyright (c) 2014年 Enn. All rights reserved.
//

#import "EnnRDBViewController.h"
#import "DNSDiscovery.h"
#import "PulsingHaloLayer.h"
#import "RNFrostedSidebar.h"
#import "RTSpinKitView.h"
#import "EnnRDBDetailViewController.h"
#import "EnnRDBTagsViewController.h"

@interface EnnRDBViewController () {
    

}

@property (weak, nonatomic) IBOutlet UILabel *coldLabel;
@property (weak, nonatomic) IBOutlet UILabel *heatLabel;
@property (weak, nonatomic) IBOutlet UILabel *eleLabel;
@property (weak, nonatomic) IBOutlet UILabel *gasLabel;

@end

@implementation EnnRDBViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    NSString *remoteAddress =  [[SAMCache sharedCache] objectForKey:RemoteServerAddress];
    if (!remoteAddress) {
        [self performSegueWithIdentifier:@"ShowDNSDiscovery" sender:self];
    } else {
        [self loadGenData];
    }
    
}


- (IBAction)reloadGenData:(UIButton *)sender {
    [self loadGenData];
}


- (IBAction)showDetail:(id)sender {
    NSArray *images = @[
                        [UIImage imageNamed:@"MainPanel"],
                        [UIImage imageNamed:@"DetailTag"],
                        [UIImage imageNamed:@"Chart"],
                        [UIImage imageNamed:@"Config"]
                        ];
    NSArray *borderColors = @[
                              [UIColor whiteColor],
                              [UIColor whiteColor],
                              [UIColor whiteColor],
                              [UIColor whiteColor]
                              ];
    
    RNFrostedSidebar *callout = [[RNFrostedSidebar alloc] initWithImages:images selectedIndices:Nil borderColors:borderColors];
    [callout setBorderWidth:1];
    [callout setTintColor:[UIColor colorWithRed:0/255.0 green:100/255.0 blue:169/255.0 alpha:0.7]];
    callout.delegate = self;
    [callout show];
}


- (IBAction)setRemoteURL:(UIBarButtonItem *)sender {
    [self performSegueWithIdentifier:@"ShowDNSDiscovery" sender:self];
}



- (void)loadGenData
{
    NSURL *url = [[DNSDiscovery sharedInstance] getGenDataURL];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSError *e = nil;
        NSDictionary *dataDict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableLeaves error:&e];
        self.coldLabel.text = [dataDict valueForKey:@"cold"];
        self.heatLabel.text = [dataDict valueForKey:@"heat"];
        self.eleLabel.text = [dataDict valueForKey:@"ele"];
        self.gasLabel.text = [dataDict valueForKey:@"gas"];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"%@", error);
    }];
    [operation start];
}


#pragma - RNFrostedSidebarDelegate

- (void)sidebar:(RNFrostedSidebar *)sidebar didTapItemAtIndex:(NSUInteger)index {
    [sidebar.view setUserInteractionEnabled:NO];
    
    double delayInSeconds = 0.5;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        [sidebar dismissAnimated:YES];
        if (index == 0) {
            EnnRDBDetailViewController *evc = [self.storyboard instantiateViewControllerWithIdentifier:@"DetailVC"];
            [self.navigationController pushViewController:evc animated:YES];
        }else if (index == 1) {
            EnnRDBTagsViewController *tagvc = [self.storyboard instantiateViewControllerWithIdentifier:@"TagVC"];
            [self.navigationController pushViewController:tagvc animated:YES];
        }
    });
    
}

@end
