//
//  main.m
//  EnnRDBMobile
//
//  Created by wang david on 14-2-17.
//  Copyright (c) 2014年 Enn. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "EnnRDBAppDelegate.h"

int main(int argc, char * argv[])
{
  @autoreleasepool {
      return UIApplicationMain(argc, argv, nil, NSStringFromClass([EnnRDBAppDelegate class]));
  }
}
